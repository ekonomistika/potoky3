import java.util.ArrayList;

class BrokerThread extends Thread {
    public  ArrayList<Trader> name;

    public BrokerThread(ArrayList<Trader> name) {
        this.name = name;
    }

    public  void setName(ArrayList<Trader> name) {
        this.name = name;
    }

    @Override
    public void run() {
        for (Trader nameoftrader : name) {

            ArrayList<Stock> nameStock = nameoftrader.getStock();
            Market market = new Market();
            ArrayList<Stock> marketStock = market.getStock();

            try {
                for (Stock namestock : nameStock) {
                    for (Stock marketstock : marketStock) {
                        if (namestock.name == marketstock.name || namestock.price > marketstock.price) {

                            System.out.println("Трейдер " + nameoftrader.getName() + " покупает акции " + marketstock.name);

                        } else {
                            System.out.println("Трейдер " + nameoftrader.getName() + " не покупает акции " + marketstock.name);
                        }
                    }
                    sleep(2000);
                }

            } catch (InterruptedException exception) {
            }
        }
    }
}