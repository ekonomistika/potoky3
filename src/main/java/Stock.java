import jdk.dynalink.beans.StaticClass;

public class Stock {
    String name;
    Integer price;
    Integer amount;

    public Stock(String name, Integer price, Integer amount) {
        this.name = name;
        this.price = price;
        this.amount = amount;

    }

    public String getName() {
        return name;
    }

    public Integer getPrice() {
        return price;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }
}
