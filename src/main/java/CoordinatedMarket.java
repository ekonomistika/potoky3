import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;

public class CoordinatedMarket {

    public static void main(String[] args) {

        Trader traderAlice      = new Trader("Alice");
        Trader traderBob        = new Trader("Bob");
        Trader traderCharlie    = new Trader("Charlie");

        traderAlice.setStock(new Stock("AAPL", 10, 100));
        traderAlice.setStock(new Stock("COKE",20, 390));

        traderBob.setStock(new Stock("AAPL", 10, 140));
        traderBob.setStock(new Stock("IBM", 20, 135));

        traderCharlie.setStock(new Stock("COKE", 300, 370));

        Trader[] traders = {traderAlice, traderBob, traderCharlie};

        BrokerThread changePrice = new BrokerThread(new ArrayList<>(Arrays.asList(traders)));

        changePrice.start();

        ChangePricesThread changePricesThread = new ChangePricesThread(new ArrayList<>(Arrays.asList(traders)));

        changePricesThread.start();

        System.out.println(LocalTime.now() + ": Початок виконання");
        
        for (int i = 0; i < 3; i++) {
            
            try {
                Thread.sleep(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        }
    }









