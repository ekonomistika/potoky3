import java.util.ArrayList;
import java.util.function.Consumer;

public class ChangePricesThread extends Thread{

    ArrayList<Trader> listTrader;

    public ChangePricesThread(ArrayList<Trader> listTrader) {
        this.listTrader = listTrader;
    }

   public void run(){
        try {
            this.listTrader.forEach(new Consumer<Trader>() {
                @Override
                public void accept(Trader trader) {
                    for (int i = 0; i < trader.stock.size(); i++) {
                        Stock st = trader.stock.get(i);
                        Integer oldPrice = st.getPrice();
                        Integer newPrice = oldPrice * (int) Math.random() * 3;
                        st.setPrice(newPrice);
                        System.out.println("Изменение цены для брокера" + trader.name +
                        " на акцию " + st.name);
                    }
                }
            });
            sleep(1000);
        }catch(InterruptedException exception){}
    }

}
