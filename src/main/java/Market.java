import java.util.ArrayList;

public class Market {

    public    ArrayList<Stock> stock = new ArrayList<Stock>(3);

    public Market() {
        this.stock.add(new Stock("AAPL", 100, 141));
        this.stock.add(new Stock("COKE", 1000, 387));
        this.stock.add(new Stock("IBM", 200, 137));
    }

    public ArrayList<Stock> getStock() {
        return stock;
    }

}
